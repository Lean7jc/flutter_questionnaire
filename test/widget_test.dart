// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_questionnaire/main.dart';

void main() {
  testWidgets('Flutter Questionnaire', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget( App());
    
    expect(find.text('Asset Management'), findsOneWidget);
    await tester.tap(find.text('Asset Management'));
    await tester.pump();

    expect(find.text('More than 10,000'), findsOneWidget);
    await tester.tap(find.text('More than 10,000'));
    await tester.pump();

    expect(find.text('Europe'), findsOneWidget);
    await tester.tap(find.text('Europe'));
    await tester.pump();

    expect(find.text('6-9 months'), findsOneWidget);
    await tester.tap(find.text('6-9 months'));
    await tester.pump();

    });
    
    
  
}
