    import 'package:flutter/material.dart';
    import 'package:flutter_questionnaire/body_questions.dart';
    import 'package:flutter_questionnaire/body_answers.dart';
    // import './body_questions.dart';

    void main() {
    //the App{} is the root widget.
    runApp(App());
    }

    class App extends StatefulWidget {
    @override
    AppState createState() => AppState();
    }

    class AppState extends State<App>{
    int questionIdx = 0;
    bool showAnswers = false;

    final questions = [
    {
        'question': 'What is the nature of your business needs?',
        'options' :  ['Time Tracking', 'Asset Management', 'Issue Tracking']
    },
    {
        'question': 'What is the expected size of the user base?',
        'options': ['Less than 1000', 'Less then 10,000', 'More than 10,000']
    },
    {
        'question': 'In which region would the majority of the users base be?',
        'options': ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East']
    },
    {
        'question': 'What is the expected project duration?',
        'options': ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months']
    },
    ];
    
    var answers =[];


    void nextQuestion(String? answer){
        answers.add({
            'question':questions[questionIdx]['question'],
            'answer': (answer == null)? '' : answer
        });
        print(answers);
        if (questionIdx < questions.length - 1){
            setState(() =>questionIdx++);
        }else {
            setState(() => showAnswers = true);
        }
    }


    @override
    Widget build(BuildContext context) {
        var bodyQuestions = BodyQuestions(questions: questions, questionIdx: questionIdx, nextQuestion: nextQuestion);
        var bodyAnswers = BodyAnswers(answers: answers);

    Scaffold homepage = Scaffold(
        appBar: AppBar(title: Text('Homepage')),
        body: (showAnswers) ? bodyAnswers :bodyQuestions
        );
        return MaterialApp(
        home: homepage,
        );
      }
    }

    //to specify margin/padding spacing we can use EdgeInsets
    //the values for spacing are in multiple of 8
    //to add spacing on all sides, use EdgeInsets.all()
    // to add spacing on certain side, use EdgeInsets.only(direction: value)

    //  In flutter, width = double.infinity is the equivalent of width = 100%
    // to put colors on a container, the decoration: BoxDecoration: Colors.green) can be used.

    // In a column widget, the main axis aligment is vertical {or from top to bottom}
    //to change the horizontal alignment of widgets in a column, we must use the crosAxisAlignment
    //for example, if we want to place column widgets to the horizontal left, we use crossAxisAlignment.star,
    //in the context of the colums, we can consider the columns as vertical, and its cross axis as horizontal.

    //  AnswerButton(nextQuestion: nextQuestion,text: answers[answerIdx][0]),
        //  AnswerButton(nextQuestion: nextQuestion, text: answers[answerIdx][1]),
        //  AnswerButton(nextQuestion: nextQuestion, text: answers[answerIdx][2]),

    //  void nextAnswer() {
    //   setState(() => answerIdx++);
    //   print(answerIdx);
    // }

    // The scafold widget provides basic design layout structure.
    //the scafold can given UI elements such as an app bar.

        // List <String> questions = [
    //   'What is the nature of your business needs?',
    //   'What is the expected size of the user base?',
    //   'In which region would the majority of the users base be?',
    //   'What is the expected project duration?'
    // ];

    // List <List<String>> answers = [
    //   ['Time Tracking', 'Asset Management', 'Issue Tracking'],
    //   ['Less than 1000', 'Less then 10,000', 'More than 10,000'],
    //   ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East'],
    //   ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months'],
    // ];
// app.questioIdx =
// bodyQuestion.questionIdx = 1