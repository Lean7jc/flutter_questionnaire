import 'package:flutter/material.dart';

class AnswerButton extends StatelessWidget {
  final String text;
  final Function nextQuestion;
  //final Function nextAnswer;

  AnswerButton({
    required this.text,
    required this.nextQuestion,
    //required this.nextAnswer
  });
  @override
  Widget build(BuildContext context) {
    return RadioListTile<String>(
      title: Text(text),
      value: text,
      groupValue: null,
      onChanged: (value) {
        print(value);
        SnackBar snackBar = SnackBar(
          content: Text('You have selected $value'),
          duration: Duration(milliseconds: 250),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        nextQuestion(value);
        //nextAnswer();
      },
    );
  }
}
